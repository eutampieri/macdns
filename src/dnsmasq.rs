use std::str::FromStr;

pub struct DnsmasqLeaseFile(String);

impl FromStr for DnsmasqLeaseFile {
    type Err = core::convert::Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(s.to_string()))
    }
}

impl From<DnsmasqLeaseFile> for std::collections::HashMap<macaddr::MacAddr, Vec<std::net::IpAddr>> {
    fn from(n: DnsmasqLeaseFile) -> Self {
        let mut res = std::collections::HashMap::new();
        n.0.split('\n')
            .map(|x| x.split(' '))
            .map(|mut x| (x.nth(1), x.next()))
            .filter(|(a, b)| a.is_some() && b.is_some())
            .map(|(a, b)| {
                (
                    macaddr::MacAddr::from_str(a.unwrap()),
                    std::net::IpAddr::from_str(b.unwrap()),
                )
            })
            .filter(|(a, b)| a.is_ok() && b.is_ok())
            .map(|(a, b)| (a.unwrap(), b.unwrap()))
            .for_each(|(mac, ip)| {
                if !res.contains_key(&mac) {
                    res.insert(mac, vec![]);
                }
                res.get_mut(&mac).unwrap().push(ip);
            });
        res
    }
}
