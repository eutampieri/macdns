use std::str::FromStr;

pub struct IpNeighDump(String);

impl FromStr for IpNeighDump {
    type Err = core::convert::Infallible;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Self(s.to_string()))
    }
}

impl IpNeighDump {
    #[cfg(target_os = "linux")]
    pub fn get() -> Self {
        Self(
            std::process::Command::new("ip")
                .arg("neigh")
                .output()
                .map(|x| String::from_utf8(x.stdout).unwrap_or_default())
                .unwrap_or_default(),
        )
    }
}

impl From<IpNeighDump> for std::collections::HashMap<macaddr::MacAddr, Vec<std::net::IpAddr>> {
    fn from(n: IpNeighDump) -> Self {
        let mut res = std::collections::HashMap::new();
        n.0.split('\n')
            .map(|x| x.split(' '))
            .map(|mut x| (x.next(), x.nth(3)))
            .filter(|(a, b)| a.is_some() && b.is_some())
            .map(|(a, b)| {
                (
                    macaddr::MacAddr::from_str(b.unwrap()),
                    std::net::IpAddr::from_str(a.unwrap()),
                )
            })
            .filter(|(a, b)| a.is_ok() && b.is_ok())
            .map(|(a, b)| (a.unwrap(), b.unwrap()))
            .for_each(|(mac, ip)| {
                if !res.contains_key(&mac) {
                    res.insert(mac, vec![]);
                }
                res.get_mut(&mac).unwrap().push(ip);
            });
        res
    }
}
