mod arp;
mod dnsmasq;

fn main() {
    let mut buf: [u8; 512] = [0; 512];
    let s = std::net::UdpSocket::bind("[::1]:55353").expect("Could not listen");
    loop {
        if let Ok((len, sender)) = s.recv_from(&mut buf) {
            if let Ok(query) = simple_dns::Packet::parse(&buf[0..len]) {
                let mut reply = simple_dns::PacketBuf::new(
                    simple_dns::PacketHeader::new_reply(query.header.id, query.header.opcode),
                    false,
                );
                let ip_neigh = std::collections::HashMap::from(arp::IpNeighDump::get());
                let leases = std::collections::HashMap::from(
                    std::fs::read_to_string("/tmp/dhcp.leases")
                        .unwrap_or_default()
                        .parse::<dnsmasq::DnsmasqLeaseFile>()
                        .unwrap(),
                );
                for query in query.questions {
                    if query.qtype != simple_dns::QTYPE::TYPE(simple_dns::TYPE::A)
                        && query.qtype != simple_dns::QTYPE::TYPE(simple_dns::TYPE::AAAA)
                    {
                        continue;
                    }
                    if let Ok(macaddr) = query.qname.get_labels()[0]
                        .to_string()
                        .parse::<macaddr::MacAddr>()
                    {
                        if let Some(ips) = ip_neigh.get(&macaddr).or_else(|| leases.get(&macaddr)) {
                            for ip in ips {
                                reply
                                    .add_answer(&simple_dns::ResourceRecord::new(
                                        query.qname.clone(),
                                        simple_dns::CLASS::IN,
                                        60,
                                        match ip {
                                            std::net::IpAddr::V4(v4) => {
                                                simple_dns::rdata::RData::A(
                                                    simple_dns::rdata::A::from(*v4),
                                                )
                                            }
                                            std::net::IpAddr::V6(v6) => {
                                                simple_dns::rdata::RData::AAAA(
                                                    simple_dns::rdata::AAAA::from(*v6),
                                                )
                                            }
                                        },
                                    ))
                                    .unwrap();
                            }
                        }
                    }
                }
                s.send_to(&reply, sender).expect("Couldn't send answer");
            }
        }
    }
}
